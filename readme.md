# fastIO

Библиотека Arduino для работы с выводами GPIO для плат Arduino на **Atmega328**. Эта реализация работает примерно в 2,5 раза быстрее по сравнению со стандартными функциями.



## Установка

### Используя Arduino IDE Library Manager

1. Откройте Менеджер библиотек `Скетч > Подключить библиотеку > Управлять библиотеками...` (`Ctrl+Shift+I`).
2. Введите `fastIO ` в поле поиска.
3. Выберите строку с библиотекой.
4. Выберите версию и щелкните кнопку `Установка`.

### Используя Git

```sh
cd ~/Documents/Arduino/libraries/
git clone https://gitlab.com/riva-lab/fastIO  fastIO
```



## Программный интерфейс

Библиотека имеет функции начинающиеся с префикса `fast`. Эти функции аналогичны таким же стандартным без префикса, но работают быстрее на Atmega328 (и только на нем!).

```c++
void    fastPinMode     (uint8_t pin, uint8_t mode);
void    fastDigitalWrite(uint8_t pin, uint8_t state);
uint8_t fastDigitalRead (uint8_t pin);
void    fastShiftOut    (uint8_t dataPin, uint8_t clockPin, uint8_t bitOrder, uint8_t value);
uint8_t fastShiftIn     (uint8_t dataPin, uint8_t clockPin, uint8_t bitOrder);
```

Также библиотека определяет макрос `FAST_IO_IMPLEMENTED`. Его можно использовать для реализации быстрого переключения на стандартную реализацию. Вставьте в свой код после `#include ...` следующую конструкцию:

```c++
#ifndef FAST_IO_IMPLEMENTED
    #define fastPinMode         pinMode
    #define fastDigitalWrite    digitalWrite
    #define fastDigitalRead     digitalRead
    #define fastShiftOut        shiftOut
    #define fastShiftIn         shiftIn
#endif
```

Чтобы быстро переключиться на стандартную реализацию этих функций, просто закомментируйте подключение библиотеки:

```с++
// #include "fastIO.h"
```

или 

```с++
/* #include "fastIO.h" */
```



## Примеры

##### Классическая мигалка

```c++
#include "fastIO.h"

void setup() {
  fastPinMode(13, OUTPUT);
}

void loop() {
  fastDigitalWrite(13, 1);
  delay(1000);
  fastDigitalWrite(13, 0);
  delay(1000);
}
```

Flash / RAM — 640 / 9 байт (стандартные функции — 924 / 9).



## Лицензия

Библиотека распространяется под [лицензией](license) [MIT](https://opensource.org/licenses/mit-license.php).



